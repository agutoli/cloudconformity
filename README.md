# NO OFFICIAL (WIP)

POC/suggestion for Cloud Conformity API

## Example

```js
const CloudConformityV1 = require('cloudconformity/v1');

const api = new CloudConformityV1({
  region: 'ap-southeast-2',
  apiKey: "<your_api_key>"
});

// promises
api.users.whoami()
  .then(({ data }) => {
    console.log(data)
  }).catch(err => console.log(err))

api.accounts.create({ ... })
  .then(({ data }) => {
    console.log(data)
  }).catch(err => console.log(err))

// async/await
const data = await api.users.whoami();
const res = await api.accounts.create({ ... });
```
