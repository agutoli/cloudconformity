import * as HttpStatus from './httpstatuses';
import { Users } from './Users';
import { Accounts } from './Accounts';

interface ApiOptions {
  region: string;
  apiKey: string;
  version: string;
};

export class CloudConformity {
  users: any;
  accounts: any;
  constructor(opts?: ApiOptions) {
    opts.version = 'v1';
    this.users = new Users(opts);
    this.accounts = new Accounts(opts);
  }
};
