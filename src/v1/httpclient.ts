import axios from 'axios';

export class HttpClient {
  axios: any;
  constructor(opts: any) {
    this.axios = axios.create({
      baseURL: `https://${opts.region}-api.cloudconformity.com/${opts.version}`,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': `ApiKey ${opts.apiKey}`
      }
    });
  }

  GET(endpoint: string) {
    return this.axios.get(endpoint);
  }

  POST(endpoint: string) {
    throw Error('not implemented');
  }

  DELETE() {
    throw Error('not implemented');
  }

  PATCH() {
    throw Error('not implemented');
  }
}
