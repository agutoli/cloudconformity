import axios from 'axios';

import { BaseApi } from '../baseapi';
import { AccountsModel } from './AccountsModel';

export class Accounts extends BaseApi {
  async get(accountId: string) {
    return this.http.GET(`/accounts/${accountId}`)
      .then(rawData => {
        return new AccountsModel(rawData)
      })
  }

  async create() {
    return this.http.POST('/accounts');
  }

  async delete(accountId: string) {
    return this.http.DELETE(`/accounts/${accountId}`);
  }
}
