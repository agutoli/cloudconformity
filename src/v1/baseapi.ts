import { HttpClient } from './httpclient';

interface ApiOptions {
  version: string;
  apiKey: string;
};

export class BaseApi {
  http: any;
  constructor(opts: ApiOptions) {
    this.http = new HttpClient(opts);
  }
}
