import axios from 'axios';
import { BaseApi } from '../baseapi';
import { UsersModel } from './UsersModel';

export class Users extends BaseApi {
  whoami() {
    return this.http.GET(`/users/whoami`);
  }
}
